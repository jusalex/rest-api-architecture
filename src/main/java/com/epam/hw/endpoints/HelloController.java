package com.epam.hw.endpoints;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Open rest";
    }

    @RequestMapping("/api/secured")
    public String api() {
        return "secured";
    }

}
