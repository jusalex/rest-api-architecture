package com.epam.hw.endpoints;

import java.util.Collections;
import java.util.List;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.hw.model.Address;
import com.epam.hw.model.User;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;
import io.swagger.annotations.ResponseHeader;

@RestController
@RequestMapping("/api/secured")
@Api(value = "/api/secured/users", description = "Users api",
        authorizations = @Authorization(value = "basic", scopes = {
                @AuthorizationScope(scope = "users", description = "Manage users")
        }), tags = "user")
public class UserController {

    @RequestMapping("/v2/users")
    @ApiOperation(value = "List of users", notes = "Returns list of users", responseHeaders = @ResponseHeader(
            name = "application/json"
    ))
    public HttpEntity<List<User>> users() {
        User user = getUser();
        return new ResponseEntity<>(Collections.singletonList(user), HttpStatus.OK);
    }

    @RequestMapping("/v1/users")
    @ApiOperation(value = "List of users", notes = "Returns list of users", responseHeaders = @ResponseHeader(
            name = "application/json"
    ))
    public List<User> getUsersV1() {
        return Collections.singletonList(getUser());
    }

    @RequestMapping(value = "/users", headers = {
            "Accept=application/vnd.company.app-3.0+json"
    })
    @ApiOperation(value = "List of users v3", notes = "Returns list of users", responseHeaders = @ResponseHeader(
            name = "application/json"
    ))
    public String getUsersV3() {
        return "{\"user\":\"v3\"}";
    }

    @RequestMapping(value = "/users", headers = {
            "Accept=application/vnd.company.app-4.0+json"
    })
    @ApiOperation(value = "List of users v4", notes = "Returns list of users", responseHeaders = @ResponseHeader(
            name = "application/json"
    ))
    public String getUsersV4() {
        return "{\"user\":\"v4\"}";
    }

    private User getUser() {
        User user = new User();
        user.setUserId(1L);
        user.setName("User");
        user.add(new Link("/users/1"));
        Address address = new Address();
        address.setId(1L);
        address.setStreet("Street");
        user.setAddresses(Collections.singletonList(address));
        return user;
    }
}
