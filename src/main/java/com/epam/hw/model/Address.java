package com.epam.hw.model;

import io.swagger.annotations.ApiModelProperty;

public class Address {
    @ApiModelProperty(notes = "Address identity")
    private Long id;
    @ApiModelProperty(notes = "Address street")
    private String street;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
