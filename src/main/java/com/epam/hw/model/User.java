package com.epam.hw.model;

import java.util.List;

import org.springframework.hateoas.ResourceSupport;

import io.swagger.annotations.ApiModelProperty;

public class User extends ResourceSupport {

    @ApiModelProperty(notes = "User identity")
    private Long userId;
    @ApiModelProperty(notes = "User name")
    private String name;
    @ApiModelProperty(notes = "User addresses")
    private List<Address> addresses;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }
}
